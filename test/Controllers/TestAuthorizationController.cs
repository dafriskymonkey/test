﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace test.Controllers
{
    [RoutePrefix("api/testauthorization")]
    public class TestAuthorizationController : ApiController
    {
        [Route("")]
        [Authorize]
        public IHttpActionResult Get() {
            return Ok("success");
        }
    }
}
