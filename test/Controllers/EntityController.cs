﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace test.Controllers
{
    public class Entity {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CreateEntityData
    {
        public string Name { get; set; }
    }

    [RoutePrefix("api/entity")]
    public class EntityController : ApiController
    {
        private static List<Entity> _entities = null;

        public EntityController()
        {
            if (_entities == null) {
                _entities = new List<Entity>
                { 
                    new Entity{
                        Id = 1,
                        Name = "first entity"
                    },
                    new Entity{
                        Id = 2,
                        Name = "second entity"
                    },
                    new Entity{
                        Id = 3,
                        Name = "third entity"
                    }
                };
            }
        }

        [Route("")]
        public IHttpActionResult GetEntities() {
            return Ok(_entities);
        }

        [Route("{entityId}")]
        public IHttpActionResult GetEntity(int entityId)
        {
            var entity = _entities.FirstOrDefault(e => e.Id == entityId);
            if (entity == null) return NotFound();

            return Ok(entity);
        }

        [Route("")]
        public IHttpActionResult CreateEntity(CreateEntityData data) {

            if (data == null) return BadRequest("data ne peut pas etre null.");
            if (string.IsNullOrEmpty(data.Name)) return BadRequest("name ne peut pas etre vide.");

            int lastId = 0;
            foreach (var entity in _entities)
            {
                if (entity.Id > lastId) lastId = entity.Id;
            }

            var newEntity = new Entity { 
                Id = lastId + 1,
                Name = data.Name
            };

            _entities.Add(newEntity);

            return Created("http://localhost:23603/api/entity/" + newEntity.Id, newEntity);
        }

        [Route("{entityId}")]
        public IHttpActionResult DeleteEntity(int entityId) {
            var entity = _entities.FirstOrDefault(e => e.Id == entityId);
            if (entity == null) NotFound();

            _entities.Remove(entity);

            return StatusCode(HttpStatusCode.NoContent);
        }

        //[Route("")]
        //public IHttpActionResult CreateEntity(Entity entity) {
        //    var existingEntity = _entities.FirstOrDefault(e => e.Id == entity.Id);
        //    if (existingEntity != null) return BadRequest("Cet id existe deja");

        //    _entities.Add(entity);

        //    return Ok();
        //}

        //[Route("test")]
        //public IHttpActionResult FirstAction() {
        //    return Ok("Post method");
        //}

        //[Route("test")]
        //public IHttpActionResult GetSecondAction() {
        //    return Ok("Get method");
        //}

        //[Route("third")]
        //public IHttpActionResult ThirdAction()
        //{
        //    return Ok("Another post method");
        //}
    }
}
