﻿using data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;

namespace test.Controllers
{
    [RoutePrefix("api/things")]
    public class ThingsController : ApiController
    {
        private ApplicationDbContext _db = null;

        public ThingsController()
        {
            if (_db == null) {
                _db = new ApplicationDbContext();
            }
        }

        [Route("")]
        public async Task<IHttpActionResult> Get() {
            var things = await _db.Things.Include(t => t.SubThing).OrderBy(t => t.Name).ToListAsync();
            return Ok(things);
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> Get(Guid id) {
            var thing = await _db.Things.Include(t => t.SubThing).FirstOrDefaultAsync(t => t.Id == id);
            if (thing == null) return NotFound();
            return Ok(thing);
        }


        public class UData {
            public string Name { get; set; }
        }

        [Route("")]
        [Authorize]
        public async Task<IHttpActionResult> Post(UData data)
        {
            var thing = new Thing { 
                Name = data.Name
            };

            _db.Things.Add(thing);
            await _db.SaveChangesAsync();

            return Created(thing.Id.ToString(), thing);
        }

        [Route("{id}")]
        [Authorize]
        public async Task<IHttpActionResult> Put(Guid id, UData data)
        {
            var thing = await _db.Things.FirstOrDefaultAsync(t => t.Id == id);
            if (thing == null) return NotFound();

            thing.Name = data.Name;
            await _db.SaveChangesAsync();
            return Ok(thing);
        }

        [Route("{id}")]
        [Authorize]
        public async Task<IHttpActionResult> Delete(Guid id) {
            var thing = await _db.Things.Include(t => t.SubThing).FirstOrDefaultAsync(t => t.Id == id);
            if (thing == null) return NotFound();

            if (thing.SubThing != null) {
                _db.SubThings.Remove(thing.SubThing);
            }

            _db.Things.Remove(thing);
            await _db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (_db != null) {
                    _db.Dispose();
                    _db = null;
                }
            }
        }
    }
}
