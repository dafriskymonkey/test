﻿(function () {
    angular.module('app', ['ngRoute', 'ngCookies', 'app.apiManager', 'app.logger', 'app.session']);

    angular.module('app').config(['$routeProvider',
    function ($routeProvider) {

        $routeProvider
            .when('/main',
                {
                    templateUrl: 'App/modules/main/main.html',
                    controller: 'mainCtrl as vm',
                    name: 'Main'
                })
            .when('/things',
                {
                    templateUrl: 'App/modules/things/things.html',
                    controller: 'thingsCtrl as vm',
                    name: 'Things'
                })
            .when('/login',
                {
                    templateUrl: 'App/modules/account/login.html',
                    controller: 'loginCtrl as vm',
                    name: 'Login'
                })
            .when('/register',
                {
                    templateUrl: 'App/modules/account/register.html',
                    controller: 'registerCtrl as vm',
                    name: 'Register'
                })
            .otherwise({ redirectTo: '/main' });
    }]);
})();