﻿(function () {
    angular.module('app').controller('loginCtrl', ['apiManager', loginCtrl]);

    function loginCtrl(apiManager) {

        var vm = this;

        vm.username = 'user2@test.com';
        vm.password = 'Password1!';

        vm.login = function () {
            apiManager.login(vm.username, vm.password);
        }
    }
})();