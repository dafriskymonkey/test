﻿(function () {
    angular.module('app').controller('registerCtrl', ['apiManager', registerCtrl]);

    function registerCtrl(apiManager) {

        var vm = this;
        vm.messageError = '';

        vm.username = 'user2@test.com';
        vm.password = 'Password1!';

        vm.register = function () {
            apiManager.register(vm.username, vm.password);
        }
    }
})();