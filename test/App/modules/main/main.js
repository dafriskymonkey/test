﻿(function () {
    angular.module('app').controller('mainCtrl', ['apiManager', mainCtrl]);

    function mainCtrl(apiManager) {

        var vm = this;

        vm.newPersonName = '';

        vm.people = [
            {
                name: 'nizar'
            },
            {
                name: 'james'
            },
            {
                name: 'john'
            }
        ];

        vm.addPerson = function () {
            vm.people.push({
                name: vm.newPersonName
            });
        }

        vm.deletePerson = function (person) {

            var tmp = [];
            for (var i = 0; i < vm.people.length; i++) {
                if (vm.people[i] != person) {
                    tmp.push(vm.people[i]);
                }
            }
            vm.people = angular.copy(tmp);
        }

        vm.authorizedApiCall = function () {
            apiManager.authorizedApiCall();
        }
    }
})();