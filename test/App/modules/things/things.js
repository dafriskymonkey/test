﻿(function () {
    angular.module('app').controller('thingsCtrl', ['apiManager', '$rootScope', 'session', thingsCtrl]);

    function thingsCtrl(apiManager,$rootScope, session) {

        var vm = this;

        vm.things = [];

        vm.newThingName = '';


        vm.createThing = function () {
            apiManager.createThing(vm.newThingName)
                .success(function () {
                    getThings();
                });
        }

        vm.deleteThing = function (thingId) {
            apiManager.deleteThing(thingId)
            .success(function () {
                getThings();
            });
        }

        getThings();

        function getThings() {
            apiManager.getThings()
            .success(function (data) {
                vm.things = angular.copy(data);
            });
        }
    }
})();