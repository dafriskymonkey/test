﻿(function () {
    angular.module('app.apiManager', [])
		.factory('apiManager', ['$http', '$cookieStore', '$location', 'logger', 'session', apiManager]);

    function apiManager($http, $cookieStore, $location, logger, session) {

        var tokenName = 'test_token';
        var getSecurityHeaders = function () {
            var token = $cookieStore.get(tokenName);
            if (!token) return {};

            return { "Authorization": "Bearer " + token };
        };
        var deleteCookie = function () {
            $cookieStore.remove(tokenName);
        }
        

        var module = {
            getThings: function () {
                return $http({
                    method: 'GET',
                    url: '/api/things'
                })
                .success(function (data) {
                    console.log(data);
                });
            },
            createThing: function (thingName) {
                return $http({
                    method: 'POST',
                    url: '/api/things',
                    headers: getSecurityHeaders(),
                    data: {
                        name: thingName
                    }
                })
                .success(function (data) {
                    console.log(data);
                });
            },
            deleteThing: function (thingId) {
                return $http({
                    method: 'DELETE',
                    url: '/api/things' + '/' + thingId,
                    headers: getSecurityHeaders()
                })
                .success(function (data) {
                    console.log(data);
                });
            },

            // authorized call
            authorizedApiCall: function(){
                return $http({
                    method: 'GET',
                    url: '/api/testauthorization',
                    headers: getSecurityHeaders()
                })
                .success(function (data) {
                    console.log(data);
                })
                .error(function (data) {
                    console.error(data);
                });
            },

            // account
            login: function (username, password) {

                var self = this;

                return $http({
                    method: 'POST',
                    url: '/token',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: 'grant_type=password&username=' + username +'&password=' + password
                })
                .success(function (data) {
                    console.log(data);

                    if (data.access_token) {
                        $cookieStore.put(tokenName, data.access_token);

                        self.getUserInfo()
                                .success(function () {
                                    logger.success('You are now logged in');
                                    $location.path('/main');
                                });
                    }
                });
            },
            register: function (username, password) {

                var self = this;

                return $http({
                    method: 'POST',
                    url: '/api/Account/Register',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    data: {
                        Email: username,
                        Password: password,
                        ConfirmPassword: password
                    }
                })
                .success(function (data) {
                    logger.success('You are registred', data, 'Success');
                    self.login(username, password);
                })
                .error(function (data) {
                    logger.error('Error',data,'error');
                });
            },
            logout: function () {

                var self = this;

                deleteCookie();
                self.userInfo = null;
                logger.success('You are Logout');
            },
            userInfo: null,
            getUserInfo: function () {

                var self = this;

                return $http({
                    method: 'GET',
                    url: '/api/account/userinfo',
                    headers: getSecurityHeaders()
                })
                .success(function (data) {
                    console.log('userInfo');
                    console.log(data);
                    self.userInfo = data;
                });
            }
        };

        return module;
    };
})();