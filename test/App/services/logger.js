(function(){
    angular.module('app.logger', [])
		.factory('logger', logger);

		function logger(){
			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "positionClass": "toast-top-right",
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}			

			return {
				error: function(message, data, title){
					toastr.error(message, title);
            		console.error('Error: ' + message, data);
				},
				info: function(message, data, title){
					toastr.info(message, title);
             		console.info('Info: ' + message, data);
				},
				success: function(message, data, title){
					toastr.success(message, title);
            		console.log('Success: ' + message, data);
				},
				warning: function(message, data, title){
					toastr.warning(message, title);
            		console.warn('Warning: ' + message, data);
				}
			};
		};
})();