﻿(function () {
    angular.module('app').controller('menuCtrl', ['$rootScope', 'apiManager', menuCtrl]);

    function menuCtrl($rootScope, apiManager) {

        var vm = this;

        vm.userInfo = apiManager.userInfo;
        vm.logout = function () {
            apiManager.logout();
        }

        $rootScope.$watch(function () {
            return apiManager.userInfo;
        }, function (value) {
            vm.userInfo = value;
        });
        apiManager.getUserInfo();
    }
})();