﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace data.Models
{
    public class Thing
    {
        public Thing()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        public Guid? SubThingId { get; set; }

        [ForeignKey("SubThingId")]
        public SubThing SubThing { get; set; }

        public DbGeography Position { get; set; }
    }
}
