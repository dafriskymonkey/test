﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace data.Models
{
    public class SubThing
    {
        public SubThing()
        {
            Id = Guid.NewGuid();
            CreatedDate = DateTimeOffset.UtcNow;
        }

        public Guid Id { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string Description { get; set; }
    }
}
