namespace data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using data.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<data.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(data.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Things.AddOrUpdate(
                    t => t.Name,
                    new Thing
                    {
                        Name = "thing 1",
                        SubThing = new SubThing
                        {
                            Description = "subthing 1 from seed"
                        }
                    },
                    new Thing
                    {
                        Name = "thing 2",
                        SubThing = new SubThing
                        {
                            Description = "subthing 2 from seed"
                        }
                    },
                    new Thing
                    {
                        Name = "thing 3",
                        SubThing = new SubThing
                        {
                            Description = "subthing 1 from seed"
                        }
                    }
                );
        }
    }
}
